export const isAdmin = () => {
  if (localStorage.getItem('token') && localStorage.getItem('role') === 'admin') {
    return true;
  }

  return false;
}
export const isEmployee = () => {
  if (localStorage.getItem('token') && localStorage.getItem('role') === 'employee') {
    return true;
  }

  return false;
}
export const isLogin = () => {
  if (localStorage.getItem('token')) {
    return true;
  }

  return false;
}