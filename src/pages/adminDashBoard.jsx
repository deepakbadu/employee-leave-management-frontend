/* eslint-disable react-hooks/rules-of-hooks */
import { Link } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Card, Table } from 'react-bootstrap'
import axios from 'axios'
import '../App.css'

function adminDashBoard() {
  const [departmentCount, setDepartmentCount] = useState({})
  const [employeeCount, setEmployeeCount] = useState({})
  const [leaveTypeCount, setLeaveTypeCount] = useState({})
  const retriveDepartmentCount = async () => {
    const response = await axios.get('department-count')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveApplication = async () => {
      const departments = await retriveDepartmentCount()
      if (departments) {
        setDepartmentCount(departments[0])
      }
    }
    getAllLeaveApplication()
  }, [])
  const retriveTotalEmployee = async () => {
    const response = await axios.get('employee-count')
    return response.data.data
  }

  useEffect(() => {
    const getAllEmployeeCount = async () => {
      const employees = await retriveTotalEmployee()
      if (employees) {
        setEmployeeCount(employees[0])
      }
    }
    getAllEmployeeCount()
  }, [])
  const retriveLeaveType = async () => {
    const response = await axios.get('leave-type-count')
    return response.data.data
  }

  useEffect(() => {
    const getLeaveTypeCount = async () => {
      const leaveType = await retriveLeaveType()
      if (leaveType) {
        setLeaveTypeCount(leaveType[0])
      }
    }
    getLeaveTypeCount()
  }, [])
  const [leaveApplications, setLeaveApplications] = useState([])
  const retriveLeaveApplication = async () => {
    const response = await axios.get('leave-application')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveApplication = async () => {
      const allEmployee = await retriveLeaveApplication()
      if (allEmployee) {
        setLeaveApplications(allEmployee)
      }
    }
    getAllLeaveApplication()
  }, [])
  return (
    <>
      <Container>
        <Row>
          <Col xs={6} md={4}>
            <Card style={{ width: '18rem' }} className='mt-2' >
              <Card.Body>
                <Card.Title className='text-center'> Total Leave Type </Card.Title>
                <Card.Text className='text-center'>
                  {leaveTypeCount.numberOfLeaveType}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card style={{ width: '18rem' }} className='mt-2' >
              <Card.Body>
                <Card.Title className='text-center'> Total Employees </Card.Title>
                <Card.Text className='text-center'>
                  {employeeCount.numberOfEmployee}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card style={{ width: '18rem' }} className='mt-2' >
              <Card.Body>
                <Card.Title className='text-center'> Total Departments </Card.Title>
                <Card.Text className='text-center'>
                  {departmentCount.totalDepartment}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Card className='mt-3'>
          <Card.Title className="p-2">Leatest Leave Applications</Card.Title>
          <Card.Body>
            <Table responsive="lg">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Leave Type</th>
                  <th>Posted Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {leaveApplications.map(leaveApplication => (
                  <tr key={leaveApplication.id}>
                    <td> {leaveApplication.name} </td>
                    <td> {leaveApplication.leave_type} </td>
                    <td> {leaveApplication.reg_date} </td>
                    <td className={leaveApplication.status === 'Approved' ? 'text-success' : 'text-danger'} > {leaveApplication.status} </td>
                    <td>
                      <Link className="" to={{ pathname: `/application/${leaveApplication.id}` }}> view </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </Container>
    </>
  )
}

export default adminDashBoard
