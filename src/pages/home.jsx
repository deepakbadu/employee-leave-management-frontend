/* eslint-disable react-hooks/rules-of-hooks */
import LoginForm from '../components/LoginForm'
import {
  BrowserRouter as Router,
} from "react-router-dom";
import '../App.css'

function home(props) {
  return (
    <>
      <Router>
        <LoginForm />
      </Router>
    </>
  )
}

export default home
