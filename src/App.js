import './App.css';
import Navbar from './components/navbar'
import Home from './pages/home'
import AdminDashboard from './pages/adminDashBoard'
import PrivateRoute from './components/privateRoute'
import React, { useEffect, useState } from 'react'
import AddEmployee from './components/AddEmployee'
import {
  Col,
  Container,
  Row,
} from 'react-bootstrap';
import axios from 'axios'
import Sidebar from './components/Sidebar'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import EmployeeList from './components/EmployeeList'
import GetAllLeave from './components/leave-types/GetAllLeave'
import AddLeave from './components/AddLeave'
import LeaveTypes from './components/LeaveTypes'
import GetLeaveApplication from './components/leave-types/GetLeaveApplication'
import PendingLeave from './components/leave-types/GetPendingLeave'
import ApprovedLeave from './components/leave-types/GetApprovedLeave'
import NotApprovedLeave from './components/leave-types/GetNotApprovedLeave'
import { isAdmin, isEmployee } from './utils';
import AdminSidebar from './components/AdminSidebar'
import EmployeeDashboard from './components/employees/EmployeeDashboard';
import AddLeaveApplication from './components/employees/AddLeaveApplication';
import ShowLeaveHistory from './components/employees/ShowLeaveHistory'
import ChangePassword from './components/employees/ChangePassword';
import AddDepartment from './components/AddDepartment'
import ShowDepartment from './components/ShowDepartments'
import EmployeeRoute from './components/EmployeeRoute';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

function App(props) {
  const [user, setUser] = useState({})
  useEffect(() => {
    axios.get('user').then(
      res => {
        setUser(res.data.data[0])
      },
      err => {
        console.log(err)
      }
    )
  }, []);

  function EmployeeDashboardA() {
    return <EmployeeDashboard />
  }
  function AddLeaveApplicationA() {
    return <AddLeaveApplication user={user} />
  }
  function showLeaveHistory() {
    return <ShowLeaveHistory user={user} />
  }
  function changePassword() {
    return <ChangePassword user={user} />
  }
  return (
    <>
      <Router>
        <Navbar user={user} />
        <ToastContainer />

        <Container fluid>
          <Row className="flex-xl-nowrap">
            <Col xs={12} md={3} lg={2} >
              {isAdmin() ? <AdminSidebar user={user} /> : isEmployee() ? < Sidebar user={user} /> : ''}
            </Col>
            <Col xs={12} md={9} lg={10}>
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <EmployeeRoute path="/employee-dashboard" component={EmployeeDashboardA} />
                <EmployeeRoute path='/change-password' component={changePassword} />
                <EmployeeRoute path='/add-leave-application' component={AddLeaveApplicationA} />
                <EmployeeRoute path='/show-leave-history' component={showLeaveHistory} />
                <PrivateRoute path='/admin-dashboard' component={AdminDashboard} />
                <PrivateRoute path='/add-employee' component={AddEmployee} />
                <PrivateRoute path='/employees' component={EmployeeList} />
                <PrivateRoute path='/all-leaves' user={user} component={GetAllLeave} />
                <PrivateRoute path='/add-leave-types' component={AddLeave} />
                <PrivateRoute path='/leave-types' component={LeaveTypes} />
                <PrivateRoute path="/application/:id" component={GetLeaveApplication} />
                <PrivateRoute path='/pending-leaves' component={PendingLeave} />
                <PrivateRoute path='/approved-leaves' component={ApprovedLeave} />
                <PrivateRoute path='/not-approved-leaves' component={NotApprovedLeave} />
                <PrivateRoute path='/add-department' component={AddDepartment} />
                <PrivateRoute path='/show-departments' component={ShowDepartment} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </Router>
    </>
  );
}

export default App;