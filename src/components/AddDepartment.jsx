import axios from 'axios'
import React, { useRef } from 'react'
import { Button, Form, Col, Card } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'

function AddLeave() {
  const departmentTypeRef = useRef()
  const history = useHistory()

  const handleChange = (e) => {
    e.preventDefault()
    const department_name = departmentTypeRef.current.value
    const data = {
      department_name,
    }
    axios.post('department', data)
      .then(res => {
        history.push('/show-departments')
      })
      .catch(err => {
        console.log(err)
      })
  }
  return (
    <>
      <Card className='mt-2' >
        <Card.Title className="px-4 pt-4">
          Add Department
        </Card.Title>
        <Card.Body>
          <Form onSubmit={handleChange} className='mx-2'>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>Department type </Form.Label>
              <Form.Control type="text" ref={departmentTypeRef} placeholder="Enter Department" />
            </Form.Group>
            <Button variant="primary" className="my-2" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default AddLeave
