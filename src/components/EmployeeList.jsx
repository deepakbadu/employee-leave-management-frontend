/* eslint-disable react-hooks/rules-of-hooks */
import axios from 'axios'
import React, { Fragment, useEffect, useState } from 'react'
import { Button, Table, Card } from 'react-bootstrap'
import * as Icon from 'react-bootstrap-icons'

function EmployeeList() {
  const [employees, setEmployees] = useState([])
  const retriveEmployee = async () => {
    const response = await axios.get('employee')
    return response.data.data
  }

  useEffect(() => {
    const getAllEmployee = async () => {
      const allEmployee = await retriveEmployee()
      if (allEmployee) {
        setEmployees(allEmployee)
      }
    }
    getAllEmployee()
  }, [])

  const removeEmployee = async (id) => {
    await axios.delete(`employee/${id}`)
    const newEmployeeList = employees.filter((employee) => {
      return employee.id !== id
    })
    setEmployees(newEmployeeList)
  }
  return (
    <>
      <Fragment>
        <Card className="mt-2">
          <Card.Title className='justify-content-end'>
          </Card.Title>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Emplyee Name</th>
                <th>Address</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {employees.map(employee => (
                <tr key={employee.id}>
                  <td>{employee.id}</td>
                  <td> {employee.name} </td>
                  <td> {employee.address} </td>
                  <td> {employee.email} </td>
                  <td> {employee.phone} </td>
                  <td>
                    <Button onClick={() => removeEmployee(employee.id)} >
                      <Icon.ArchiveFill />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      </Fragment>
    </>
  );
}

export default EmployeeList
