import 'react-bootstrap-drawer/lib/style.css';
import React, { useState } from 'react';
import image from '../assets/th.jpeg'
import '../App.css'
import {
  Collapse,
  Nav,
  Accordion,
  Card,
  Image
} from 'react-bootstrap';
import { Drawer, } from 'react-bootstrap-drawer';
import { NavLink } from 'react-router-dom'
import styled from "styled-components";

const ApplicationDrawer = (props) => {
  const linkStyle = {
    textDecoration: "none",
    color: 'blue'
  };
  const [open, setOpen] = useState(false);
  const handleToggle = () => setOpen(!open);
  const activeStyle = {
    color: 'deepskyblue'
  }
  return (
    <Drawer {...props}>
      <Drawer.Toggle onClick={handleToggle} />
      <Collapse in={open}>
        <Drawer.Overflow>
          <Drawer.ToC>
            <Image src={image} width='100' height='100' className='mx-5' fluid roundedCircle />
            <p className='mx-4'>Welcome {props.user.name} </p>
            <Accordion defaultActiveKey="0">
              <Nav.Link>
                <NavLink to="/admin-dashboard" activeStyle={activeStyle} style={linkStyle} className='text-black'>
                  Dashboard
                </NavLink>
              </Nav.Link>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  Employee
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <Nav.Link>
                      <NavLink to="/add-employee" activeStyle={activeStyle} style={linkStyle}>
                        Add Employee
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/employees" activeStyle={activeStyle} style={linkStyle}>
                        Show Employee
                      </NavLink>
                    </Nav.Link>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                  Leave Type
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>
                    <Nav.Link>
                      <NavLink to="/add-leave-types" activeStyle={activeStyle} style={linkStyle}>
                        Add Leave Type
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/leave-types" activeStyle={activeStyle} style={linkStyle}>
                        Show Leave Type
                      </NavLink>
                    </Nav.Link>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="2">
                  Department
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="2">
                  <Card.Body>
                    <Nav.Link>
                      <NavLink to="/add-department" activeStyle={activeStyle} style={linkStyle}>
                        Add Department
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/show-departments" activeStyle={activeStyle} style={linkStyle}>
                        Show Departments
                      </NavLink>
                    </Nav.Link>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="3">
                  Leave Management
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="3">
                  <Card.Body>
                    <Nav.Link>
                      <NavLink to="/all-leaves" activeStyle={activeStyle} style={linkStyle}>
                        All Leaves
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/pending-leaves" activeStyle={activeStyle} style={linkStyle}>
                        Pending Leave
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/approved-leaves" activeStyle={activeStyle} style={linkStyle}>
                        Approve Leave
                      </NavLink>
                    </Nav.Link>
                    <Nav.Link>
                      <NavLink to="/not-approved-leaves" activeStyle={activeStyle} style={linkStyle}>
                        Not Approve Leave
                      </NavLink>
                    </Nav.Link>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Nav.Link onClick={() => localStorage.clear()} href="/" style={linkStyle}>
                Logout
              </Nav.Link>
            </Accordion>
          </Drawer.ToC>
        </Drawer.Overflow>
      </Collapse>
    </Drawer>
  );
};

export default ApplicationDrawer