import { useParams } from "react-router-dom";
import { ListGroup, Card, Button, Modal, Form, Col } from 'react-bootstrap'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Moment from 'react-moment';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function GetLeaveApplication() {
  const { id } = useParams()
  const [leaveApplication, setLeaveApplication] = useState({})
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [status, setStatus] = useState('')
  const [admin_description, setDescription] = useState('')
  const [error, setError] = useState('')
  const notify = () => toast(error);
  const retriveLeaveApplication = async () => {
    const response = await axios.get(`leave-application/${id}`)
    return response.data.data
  }
  const getLeaveApplication = async () => {
    const allEmployee = await retriveLeaveApplication()
    if (allEmployee) {
      setLeaveApplication(allEmployee[0])
      setStatus(allEmployee[0].status)
      setDescription(allEmployee[0].admin_description)
    }
  }
  useEffect(() => {
    getLeaveApplication()
  }, [])
  function handleSubmit() {
    let data = {
      status,
      admin_description
    }

    axios.patch(`leave-application/${id}`, data)
      .then(res => {
        setLeaveApplication(leaveApplication)
        getLeaveApplication()
        setError('Leave Application updated Successfully')
      })
      .catch(err => {
        setError('Error while updating')
      })
  }
  return (
    <div>
      <Card className='mt-2'>
        <Card.Title className="px-4 pt-4">
          Leave Application of {leaveApplication.name}
        </Card.Title>
        <Card.Body>
          <ListGroup variant="flush">
            <ListGroup.Item> <strong>Name:</strong> {leaveApplication.name} </ListGroup.Item>
            <ListGroup.Item> <strong>Leave Type: </strong> {leaveApplication.leave_type} </ListGroup.Item>
            <ListGroup.Item> <strong>Staring Date: </strong>
              <Moment format="D MMM YYYY" withTitle>
                {leaveApplication.starting_date}
              </Moment>
            </ListGroup.Item>
            <ListGroup.Item> <strong> Ending Date: </strong>
              <Moment format="D MMM YYYY" withTitle>
                {leaveApplication.ending_date}
              </Moment>
            </ListGroup.Item>
            <ListGroup.Item> <strong> Status: </strong>  <span className={leaveApplication.status === 'Approved' ? 'text-success' : 'text-danger'}>{leaveApplication.status}</span> </ListGroup.Item>
            <ListGroup.Item> <strong> Description: </strong> {leaveApplication.emp_description} </ListGroup.Item>
            <ListGroup.Item>
              <Button onClick={handleShow}>
                Action
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card.Body>
      </Card>
      <Modal show={show} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title> Action On leave Application </Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
          <Modal.Body>
            <Form.Group as={Col} controlId="formGridState">
              <Form.Label>Status</Form.Label>
              <Form.Control as="select" value={status} onChange={(e) => setStatus(e.target.value)} >
                <option> Pending </option>
                <option>Approved</option>
                <option>Not Approved</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label> Description </Form.Label>
              <Form.Control as="textarea" type='text' value={admin_description} onChange={(e) => setDescription(e.target.value)} rows={3} />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
          </Button>
            <Button variant="primary" type="submit"
              onClick={() => {
                handleClose()
                handleSubmit()
                notify()
              }
              }
            >
              Save Changes
          </Button>
            <ToastContainer />
          </Modal.Footer>
        </Form>
      </Modal>
    </div>
  )
}

export default GetLeaveApplication
