import React, { Fragment, useEffect, useState } from 'react'
import { Container, Card, Table } from 'react-bootstrap'
import axios from 'axios'
import { Link } from 'react-router-dom'
import Moment from 'moment'
function AllLeaves() {
  const [leaveApplications, setLeaveApplications] = useState([])
  const retriveLeaveApplication = async () => {
    const response = await axios.get('not-approved-leaves')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveApplication = async () => {
      const allEmployee = await retriveLeaveApplication()
      if (allEmployee) {
        setLeaveApplications(allEmployee)
      }
    }
    getAllLeaveApplication()
  }, [])
  console.log(leaveApplications)
  return (
    <>
      <Container>
        <Card className='mt-3'>
          <Card.Title className="px-4 pt-4"> Not Approved Leave Applications</Card.Title>
          <Card.Body>
            <Table responsive="lg">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Leave Type</th>
                  <th>Posted Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {leaveApplications.map(leaveApplication => (
                  <tr key={leaveApplication.id}>
                    <td> {leaveApplication.name} </td>
                    <td> {leaveApplication.leave_type} </td>
                    <td> <Moment fromNow>{leaveApplication.reg_date}</Moment> </td>
                    <td className='text-danger'> {leaveApplication.status} </td>
                    <td>
                      <Link to={{ pathname: `/application/${leaveApplication.id}` }}> view </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </Container>
    </>
  )
}

export default AllLeaves
