import React, { useEffect, useState } from 'react'
import { Container, Card, Table } from 'react-bootstrap'
import axios from 'axios'
import { Link } from 'react-router-dom'
import Moment from 'react-moment';

function AllLeaves() {
  const [leaveApplications, setLeaveApplications] = useState([])
  const retriveLeaveApplication = async () => {
    const response = await axios.get('leave-application')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveApplication = async () => {
      const allEmployee = await retriveLeaveApplication()
      if (allEmployee) {
        setLeaveApplications(allEmployee)
      }
    }
    getAllLeaveApplication()
  }, [])
  return (
    <>
      <Container>
        <Card className='mt-3'>
          <Card.Title className="px-4 pt-4"> All Leave Applications</Card.Title>
          <Card.Body>
            <Table responsive="lg">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Leave Type</th>
                  <th>Posted Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {leaveApplications.map(leaveApplication => (
                  <tr key={leaveApplication.id}>
                    <td> {leaveApplication.name} </td>
                    <td> {leaveApplication.leave_type} </td>
                    <td> <Moment fromNow>{leaveApplication.reg_date}</Moment> </td>
                    <td className={leaveApplication.status === 'Approved' ? 'text-success' : leaveApplication.status === 'Pending' ? 'text-info' : 'text-danger'} > {leaveApplication.status} </td>
                    <td>
                      <Link to={{ pathname: `/application/${leaveApplication.id}` }}> view </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </Container>
    </>
  )
}

export default AllLeaves
