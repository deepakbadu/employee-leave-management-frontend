import axios from 'axios'
import React, { useRef } from 'react'
import { Button, Form, Col, Card } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'

function AddLeave() {
  const leaveTypeRef = useRef()
  const descriptionRef = useRef()
  const history = useHistory()

  const handleChange = (e) => {
    e.preventDefault()
    const leave_type = leaveTypeRef.current.value
    const description = descriptionRef.current.value
    const data = {
      leave_type,
      description
    }
    axios.post('leave-type', data)
      .then(res => {
        history.push('/leave-types')
      })
      .catch(err => {
        console.log(err)
      })
  }
  return (
    <>
      <Card className='mt-2' >
        <Card.Title className="px-4 pt-4">
          Add Leave Type
        </Card.Title>
        <Card.Body>
          <Form onSubmit={handleChange} className='mx-2'>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>Leave Type </Form.Label>
              <Form.Control type="text" ref={leaveTypeRef} placeholder="Enter Full Name" />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label> description </Form.Label>
              <Form.Control as="textarea" type='text' rows={3} ref={descriptionRef} />
            </Form.Group>
            <Button variant="primary" className="my-2" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default AddLeave
