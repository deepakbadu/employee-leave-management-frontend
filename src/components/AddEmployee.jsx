import axios from 'axios'
import React, { useRef, useState, useEffect } from 'react'
import { Button, Form, Col, Card, Alert } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'

function AddEmployee() {
  const nameref = useRef()
  const emailref = useRef()
  const addressref = useRef()
  const departmentref = useRef()
  const cityref = useRef()
  const birthdayref = useRef()
  const genderref = useRef()
  const phoneref = useRef()
  const history = useHistory()
  const [department, setDepartment] = useState([])
  const [response, setResponse] = useState('')

  const retriveLeaveType = async () => {
    const response = await axios.get('department')
    return response.data.data
  }

  useEffect(() => {
    const getDepartment = async () => {
      const allDepartment = await retriveLeaveType()
      if (allDepartment) {
        setDepartment(allDepartment)
      }
    }
    getDepartment()
  }, [])

  function handleChange(e) {
    e.preventDefault()
    const name = nameref.current.value
    const email = emailref.current.value
    const address = addressref.current.value
    const department = departmentref.current.value
    const city = cityref.current.value
    const birthday = birthdayref.current.value
    const gender = genderref.current.value
    const phone = phoneref.current.value
    const data = {
      name,
      email,
      address,
      department,
      city,
      birthday,
      gender,
      phone,
    }
    axios.post('employee', data)
      .then(res => {
        if (response) {
          setResponse(res.data)
        } else {
          history.push('/employees')
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  return (
    <>
      <Card className='my-2'>
        <Card.Title className="px-4 pt-4">
          Employee From
        </Card.Title>
        {response === '' ? '' :
          <Alert variant='danger'>
            {response}
          </Alert>}
        <Card.Body>
          <Form onSubmit={handleChange} className='mx-2' >
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>Employee Full Name </Form.Label>
              <Form.Control type="text" ref={nameref} placeholder="Enter Full Name" />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" ref={emailref} placeholder="Email" />
            </Form.Group>

            <Form.Row>
              <Form.Group as={Col} controlId="formGridAddress1">
                <Form.Label>Address</Form.Label>
                <Form.Control placeholder="1234 Main St" ref={addressref} />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridState">
                <Form.Label>Department</Form.Label>
                <Form.Control as="select" ref={departmentref} defaultValue="Choose...">
                  <option>Choose...</option>
                  {department.map(dep => (
                    <option> {dep.department_name}  </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formGridCity">
                <Form.Label>City</Form.Label>
                <Form.Control type="text" placeholder="Enter Password" ref={cityref} />
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formGridAddress1">
                <Form.Label>Birth Day</Form.Label>
                <Form.Control type="date" ref={birthdayref} placeholder="Date of Birth" />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridState">
                <Form.Label>Gender</Form.Label>
                <Form.Control as="select" ref={genderref} defaultValue="Choose...">
                  <option>Choose...</option>
                  <option>Male</option>
                  <option>Female</option>
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col} controlId="formGridCity">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type="text" ref={phoneref} placeholder="Enter Phone Number" />
              </Form.Group>
            </Form.Row>
            <Button variant="primary" className="mt-2" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default AddEmployee
