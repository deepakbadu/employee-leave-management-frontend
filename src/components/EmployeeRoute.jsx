
import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import { isEmployee } from '../utils';

const EmployeeRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => (
      isEmployee() ?
        <Component {...props} />
        : <Redirect to="/" />
    )} />
  );
};

export default EmployeeRoute;