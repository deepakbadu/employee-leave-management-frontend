import axios from 'axios'
import React, { Fragment, useEffect, useState } from 'react'
import { Button, Table, Card } from 'react-bootstrap'
import * as Icon from 'react-bootstrap-icons'

function LeaveTypes() {
  const [department, setDepartment] = useState([])

  const retriveLeaveType = async () => {
    const response = await axios.get('department')
    return response.data.data
  }

  useEffect(() => {
    const getDepartment = async () => {
      const allDepartment = await retriveLeaveType()
      if (allDepartment) {
        setDepartment(allDepartment)
      }
    }
    getDepartment()
  }, [])

  const removeLeaveType = async (id) => {
    await axios.delete(`department/${id}`)
    const newLeaveType = department.filter((d) => {
      return d.id !== id
    })
    setDepartment(newLeaveType)
  }
  return (
    <>
      <Fragment>
        <Card className="mt-2">
          <Card.Title className='justify-content-end'>
          </Card.Title>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>id</th>
                <th>Department Type</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {department.map(dep => (
                <tr key={dep.id}>
                  <td>{dep.id}</td>
                  <td> {dep.department_name} </td>
                  <td>
                    <Button onClick={() => removeLeaveType(dep.id)} >
                      <Icon.ArchiveFill />
                    </Button>
                  </td>
                </tr>
              ))}

            </tbody>
          </Table>
        </Card>
      </Fragment>
    </>
  )
}

export default LeaveTypes
