/* eslint-disable react-hooks/rules-of-hooks */
import React, { useRef, useState } from 'react'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

function LoginForm() {
  const emailRef = useRef()
  const passwordRef = useRef()
  const history = useHistory()
  const [error, setError] = useState('')

  function handleSubmit(e) {
    e.preventDefault()
    const email = emailRef.current.value
    const password = passwordRef.current.value
    const data = {
      email,
      password
    }
    axios.post('login', data)
      .then(res => {
        localStorage.setItem('token', res.data.token, 'role', res.data.role)
        localStorage.setItem('role', res.data.role)
        if (localStorage.getItem('role') === 'admin') {
          history.push('/admin-dashboard')
          window.location.reload()
        } else {
          history.push('employee-dashboard')
          window.location.reload()
        }
      })
      .catch(err => {
        setError('Unable to login')
      })
  }
  return (
    <>
      <Card style={{ width: '55em' }} className="my-5" bg="light">
        <Card.Title className="px-4 pt-4"> Login </Card.Title>
        {error ? < Alert variant='danger' className="mx-4 mt-2">
          {error}
        </Alert> : ''}
        <Card.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formBasicEmail" className="mx-2">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" ref={emailRef} placeholder="Email" />
            </Form.Group>
            <Form.Group controlId="formBasicPassword" className="m-2">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" ref={passwordRef} placeholder="Password" />
            </Form.Group>
            <Button variant="primary" className="m-2" type="submit">
              Login
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default LoginForm