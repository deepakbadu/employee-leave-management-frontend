import axios from 'axios'
import React, { Fragment, useEffect, useState } from 'react'
import { Button, Table, Card } from 'react-bootstrap'
import * as Icon from 'react-bootstrap-icons'

function LeaveTypes() {
  const [leaveType, setLeaveType] = useState([])

  const retriveLeaveType = async () => {
    const response = await axios.get('leave-type')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveType = async () => {
      const allLeaveType = await retriveLeaveType()
      if (allLeaveType) {
        setLeaveType(allLeaveType)
      }
    }
    getAllLeaveType()
  }, [])

  const removeLeaveType = async (id) => {
    await axios.delete(`leave-type/${id}`)
    const newLeaveType = leaveType.filter((employee) => {
      return employee.id !== id
    })
    setLeaveType(newLeaveType)
  }
  return (
    <>
      <Fragment>
        <Card className="mt-2">
          <Card.Title className='justify-content-end'>
          </Card.Title>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>id</th>
                <th>Leave Type</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {leaveType.map(leave => (
                <tr key={leave.id}>
                  <td>{leave.id}</td>
                  <td> {leave.leave_type} </td>
                  <td> {leave.description} </td>
                  <td>
                    <Button onClick={() => removeLeaveType(leave.id)} >
                      <Icon.ArchiveFill />
                    </Button>
                  </td>
                </tr>
              ))}

            </tbody>
          </Table>
        </Card>
      </Fragment>
    </>
  )
}

export default LeaveTypes
