import axios from 'axios'
import React, { useRef, useEffect, useState } from 'react'
import { Button, Form, Col, Card } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify';

function AddLeaveApplication(props) {
  const leaveTypeRef = useRef()
  const descriptionRef = useRef()
  const startingDateRef = useRef()
  const endingDateRef = useRef()
  const history = useHistory()
  const [leaveType, setLeaveType] = useState([])
  const [message, setMessage] = useState()

  const retriveLeaveType = async () => {
    const response = await axios.get('leave-type')
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveType = async () => {
      const allLeaveType = await retriveLeaveType()
      if (allLeaveType) {
        setLeaveType(allLeaveType)
      }
    }
    getAllLeaveType()
  }, [])
  function handleChange(e) {
    e.preventDefault()
    const leave_type = leaveTypeRef.current.value
    const emp_description = descriptionRef.current.value
    const starting_date = startingDateRef.current.value
    const ending_date = endingDateRef.current.value
    const emp_id = props.user.id
    const data = {
      leave_type,
      emp_description,
      starting_date,
      ending_date,
      emp_id
    }
    axios.post('leave-application', data)
      .then(res => {
        setMessage('Application submmited sucessfully. please wait for admin Approval.')
        history.push('/show-leave-history')
      })
      .catch(err => {
        setMessage('cannot post leave application')
      })
  }
  const showToast = () => {
    toast(message)
  };
  console.log(message)
  return (
    <>
      <Card className='mt-2'>
        <Card.Title className="px-4 pt-4">
          Leave Application From
          </Card.Title>
        <Card.Body>
          <Form onSubmit={handleChange} className='mx-2'>
            <Form.Group as={Col} controlId="formGridState">
              <Form.Label>Leave Type</Form.Label>
              <Form.Control as="select" ref={leaveTypeRef} defaultValue="Choose...">
                <option> Choose.... </option>
                {leaveType.map(leave => (
                  <option> {leave.leave_type} </option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label> description </Form.Label>
              <Form.Control as="textarea" type='text' rows={3} ref={descriptionRef} />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridAddress1">
              <Form.Label>Starting Date</Form.Label>
              <Form.Control type="date" ref={startingDateRef} placeholder="Date of Birth" />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridAddress1">
              <Form.Label>Ending Date</Form.Label>
              <Form.Control type="date" ref={endingDateRef} placeholder="Date of Birth" />
            </Form.Group>
            <Button variant="primary" onClick={showToast} className="mt-2" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default AddLeaveApplication
