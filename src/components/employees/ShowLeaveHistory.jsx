import React, { useEffect, useState } from 'react'
import { Card, Table } from 'react-bootstrap'
import axios from 'axios'
import Moment from 'react-moment';

function ShowLeaves(props) {
  const [leaveApplications, setLeaveApplications] = useState([])

  const retriveLeaveApplication = async () => {
    let emp_id = props.user.id
    const response = await axios.get(`individual-leave/${emp_id}`)
    return response.data.data
  }

  useEffect(() => {
    const getAllLeaveApplication = async () => {
      const allEmployee = await retriveLeaveApplication()
      if (allEmployee) {
        setLeaveApplications(allEmployee)
      }
    }
    getAllLeaveApplication()
  }, [])

  return (
    <>
      <Card className='mt-3'>
        <Card.Title className="px-4 pt-4"> Your Leave History</Card.Title>
        <Card.Body>
          <Table responsive="lg">
            <thead>
              <tr>
                <th>Leave Type</th>
                <th>To</th>
                <th>From</th>
                <th>Description</th>
                <th>Posted Date</th>
                <th>Admin Remarks</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {leaveApplications.map(leaveApplication => (
                <tr key={leaveApplication.id}>
                  <td> {leaveApplication.leave_type} </td>
                  <td>
                    <Moment format="D MMM YYYY" withTitle>
                      {leaveApplication.starting_date}
                    </Moment>
                  </td>
                  <td>
                    <Moment format="D MMM YYYY" withTitle>
                      {leaveApplication.ending_date}
                    </Moment>
                  </td>
                  <td> {leaveApplication.emp_description} </td>
                  <td> <Moment fromNow>{leaveApplication.reg_date}</Moment> </td>
                  <td> {leaveApplication.admin_description} </td>
                  <td className={leaveApplication.status === 'Approved' ? 'text-success' : 'text-danger'} > {leaveApplication.status} </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    </>
  )
}

export default ShowLeaves
