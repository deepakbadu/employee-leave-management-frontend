import { Button, Form, Col, Card, Alert } from 'react-bootstrap'
import React, { useState } from 'react'
import axios from 'axios'


function ChangePassword(props) {
  const [password, setPassword] = useState()
  const [newPassword, setNewPassword] = useState()
  const [confirmPassword, setConfirmPassword] = useState()
  const [message, setMessage] = useState()

  const handleChange = (e) => {
    e.preventDefault()
    if (newPassword === confirmPassword) {
      let id = props.user.id
      let data = {
        password,
        newPassword,
        confirmPassword
      }
      axios.patch(`change-password/${id}`, data)
        .then(res => {
          setPassword('')
          setNewPassword('')
          setConfirmPassword('')
          setMessage('Password change sucuessfully!!')
        })
        .catch(err => {
          setMessage('Currrent password does not match')
        })
    } else {
      setMessage('Confirm password does not match')
    }
  }

  return (
    <>
      <Card className='mt-2'>
        <Card.Title className="px-4 pt-4">
          Change Password
          </Card.Title>
        <Card.Body>
          {message ? <Alert> {message} </Alert>
            : ''}
          <Form onSubmit={handleChange} className='mx-2'>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label> Current Password </Form.Label>
              <Form.Control type='password' placeholder='Enter current password' value={password} onChange={(e) => setPassword(e.target.value)} />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridAddress1">
              <Form.Label>New Password</Form.Label>
              <Form.Control type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} placeholder="New Password" />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridAddress1">
              <Form.Label>Confirm New Password</Form.Label>
              <Form.Control type="password" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} placeholder="Confirm New Password" />
            </Form.Group>
            <Button variant="primary" className="my-2" type="submit">
              Change
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}

export default ChangePassword
