import React, { Fragment, useEffect, useState } from 'react'
import { Container, Card, ListGroup } from 'react-bootstrap'
import axios from 'axios'
import Moment from 'react-moment';

function EmployeeDashboard() {
  const [user, setUser] = useState({})
  const retriveUser = async () => {
    const response = await axios.get('user')
    return response.data.data[0]
  }

  useEffect(() => {
    const getLoggedInUser = async () => {
      const user = await retriveUser()
      if (user) {
        setUser(user)
      }
    }
    getLoggedInUser()
  }, [])
  return (
    <>
      <Container>
        <Card className='mt-2'>
          <Card.Title className="px-4 pt-4">
            Welcome {user.name} you can view your details
          </Card.Title>
          <Card.Body>
            <ListGroup variant="flush">
              <ListGroup.Item> <strong>Name:</strong> {user.name}  </ListGroup.Item>
              <ListGroup.Item> <strong>Email: </strong> {user.email}  </ListGroup.Item>
              <ListGroup.Item> <strong>Register Date: </strong>
                <Moment format="D MMM YYYY" withTitle>
                  {user.reg_date}
                </Moment>
              </ListGroup.Item>
              <ListGroup.Item> <strong> Address: </strong> {user.address}  </ListGroup.Item>
              <ListGroup.Item> <strong> Gender: </strong> {user.gender} </ListGroup.Item>
              <ListGroup.Item> <strong> Phone Number: </strong> {user.phone} </ListGroup.Item>
              <ListGroup.Item> <strong> Department: </strong> {user.department} </ListGroup.Item>
              <ListGroup.Item> <strong>Birth Date: </strong>
                <Moment format="D MMM YYYY" withTitle>
                  {user.birthday}
                </Moment>
              </ListGroup.Item>
              <ListGroup.Item>
              </ListGroup.Item>
            </ListGroup>
          </Card.Body>
        </Card>
      </Container>
    </>
  )
}

export default EmployeeDashboard