import 'react-bootstrap-drawer/lib/style.css';
import React, { useState } from 'react';
import {
  Collapse,
  Nav,
  Card,
  Accordion,
  Image
} from 'react-bootstrap';
import { Drawer, } from 'react-bootstrap-drawer';
import { NavLink } from 'react-router-dom';
import image from '../assets/th.jpeg'
import '../App.css'

const ApplicationDrawer = (props) => {
  const [open, setOpen] = useState(false);
  const handleToggle = () => setOpen(!open);
  const linkStyle = {
    textDecoration: "none",
    color: 'blue'
  };
  const activeStyle = {
    color: 'deepskyblue'
  }
  return (
    <Drawer {...props}>
      <Drawer.Toggle onClick={handleToggle} />
      <Collapse in={open}>
        <Drawer.Overflow>
          <Drawer.ToC>
            <Image src={image} width='100' height='100' className='mx-5' fluid roundedCircle />
            <p className='mx-4'>Welcome {props.user.name} </p>
            <Nav className='ml-auto'>
              <Nav.Link>
                <NavLink to='/employee-dashboard' activeStyle={activeStyle} style={linkStyle}>
                  Employee Dashboard
                </NavLink>
              </Nav.Link>
              <Nav.Link>
                <NavLink to="/change-password" activeStyle={activeStyle} style={linkStyle}>
                  Change Password
                </NavLink>
              </Nav.Link>
              <Card>
                <Accordion defaultActiveKey="0">
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                    Leaves
                </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <Nav.Link>
                        <NavLink to="/add-leave-application" activeStyle={activeStyle} style={linkStyle}>
                          Add Leave
                          </NavLink>
                      </Nav.Link>
                      <Nav.Link>
                        <NavLink to="/show-leave-history" activeStyle={activeStyle} style={linkStyle}>
                          Leave History
                        </NavLink>
                      </Nav.Link>
                    </Card.Body>
                  </Accordion.Collapse>
                </Accordion>
              </Card>
              <Nav.Link className='display-block' onClick={() => localStorage.clear()} style={linkStyle} href="/">
                Sign Out
              </Nav.Link>
            </Nav>
          </Drawer.ToC>
        </Drawer.Overflow>
      </Collapse>
    </Drawer>
  );
};

export default ApplicationDrawer