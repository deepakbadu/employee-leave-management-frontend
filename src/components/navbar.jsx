/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";

function navbar(props) {
  return (
    <>
      <Router>
        <Navbar collapseOnSelect expand="sm" bg="primary" variant="dark" className="px-2">
          <Navbar.Brand href="/"> Leave Management System </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className='m-auto'>
            </Nav>
            <Nav>
              {props.user.id ? <Nav.Link href="/" className='text-white' >
                {props.user.name}
              </Nav.Link> : <Nav.Link href='/' >
                <Link to='/' className='text-white fw-bold'>
                  Login
                </Link>
              </Nav.Link>}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Router>
    </>
  )
}

export default navbar
